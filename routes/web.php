<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/todos', 'TodosController@index')->name('todos');

Route::post('/todos/create', 'TodosController@store')->name('todos.create');

Route::delete('/todos/{todo}', 'TodosController@destroy')->name('todos.destroy');

Route::get('/todos/{todo}/edit', 'TodosController@edit')->name('todos.edit');

Route::put('/todos/{todo}', 'TodosController@update')->name('todos.update');

Route::get('/todos/{todo}/complete', 'TodosController@completed')->name('todos.completed');
