<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodosController extends Controller
{
    public function index()
    {
        return view('todos')->with('todos', Todo::all());
    }

    public function store(Request $request)
    {
        Todo::create([
            'todo' => $request->todo
        ]);

        session()->flash('success', 'new todo created');

        return redirect()->back();
    }

    public function destroy(Todo $todo)
    {
        $todo->delete();

        session()->flash('success', 'todo deleted');

        return redirect()->back();
    }

    public function edit(Todo $todo)
    {
        return view('edit')->with('todo' ,$todo);
    }

    public function update(Request $request, Todo $todo)
    {
        $todo->update([
            'todo' => $request->todo
        ]);

        session()->flash('success', 'todo updated');

        return redirect()->route('todos');
    }

    public function completed(Todo $todo)
    {
        $todo->update([
            'completed' => 1
        ]);

        session()->flash('success', 'todo completed');

        return redirect()->route('todos');
    }
}
