@extends('layouts.app')

@section('content')
    <div class="container text-center">
        <div class="mt-5">
            <div class="text-center text-primary display-3 mb-4">
                Todos
            </div>
            <form action="{{ route('todos.create') }}" method="POST">
                @csrf
                <div class="form-row">
                    <div class="col-md-6 offset-md-3 mb-3 justify-content-center">
                    <input name="todo" type="text" class="form-control form-control-lg" placeholder="Create a new todo">
                </div>
                </div>
            </form>
            <div class="h3 mt-3">
                @foreach($todos as $todo)
                        <form action="{{ route('todos.destroy', $todo->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                                {{ $todo->todo }}
                                <a class="btn btn-info ml-2 text-white" href="{{ route('todos.edit', $todo->id) }}">Update</a>
                                <button type="submit" class="btn btn-danger ml-2">X</button>
                                @if(!$todo->completed)
                                        <a class="btn btn-sm btn-success" href="{{ route('todos.completed', $todo->id) }}">mark as completed</a>
                                    @else
                                        completed
                                @endif

                        </form>
                    <hr>
                @endforeach
            </div>
        </div>
    </div>
@endsection
