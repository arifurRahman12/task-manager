@extends('layouts.app')

@section('content')
    <div class="container text-center">
        <div class="row mt-5 pt-5">
            <div class="col-md-6 offset-md-3">
                <form action="{{ route('todos.update', $todo->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="input-group">
                        <input name="todo" type="text" class="form-control" value="{{ $todo->todo }}">
                        <div class="input-group-append">
                            <button class="btn btn-info ml-3" type="submit">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
